package com.example;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class DoubleStreams {
    private final List<Double> list;
    private final int count;

    public DoubleStreams(List<Double> elements) {
        this.list = elements;
        this.count = elements.size();
    }

    public Double getSum() {
        return this.list.stream()
                .reduce(Double::sum)
                .get();
    }

    public Double getAvg() {
        return this.list.stream()
                .reduce(Double::sum)
                .map(s -> s / this.count)
                .get();
    }

    public List<Double> getTenPercent() {
        int tenPercent = (int) Math.ceil(count * 0.1);
        return this.list.stream()
                .sorted(Comparator.reverseOrder())
                .limit(tenPercent)
                .collect(Collectors.toList());
    }
}
