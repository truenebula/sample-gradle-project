package com.example;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class BigDecimalStreams {
    private final List<BigDecimal> list;
    private final int count;

    public BigDecimalStreams(List<BigDecimal> elements) {
        this.list = elements;
        this.count = elements.size();
    }

    public BigDecimal getSum() {
        return this.list.stream()
                .reduce(BigDecimal::add)
                .get();
    }

    public BigDecimal getAvg() {
        return this.list.stream()
                .reduce(BigDecimal::add)
                .map(s -> s.divide(new BigDecimal(this.count), 2, RoundingMode.HALF_UP))
                .get();
    }

    public List<BigDecimal> getTenPercent() {
        int tenPercent = (int) Math.ceil(count * 0.1);
        return this.list.stream()
                .sorted(Comparator.reverseOrder())
                .limit(tenPercent)
                .collect(Collectors.toList());
    }
}
