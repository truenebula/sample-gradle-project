package com.example;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class DoublePrimitiveStreams {
    private final List<Double> list;
    private final int count;

    public DoublePrimitiveStreams(List<Double> elements) {
        this.list = elements;
        this.count = elements.size();
    }

    public double getSum() {
        return this.list.stream()
                .mapToDouble(Double::doubleValue)
                .sum();
    }

    public double getAvg() {
        return this.list.stream()
                .mapToDouble(Double::doubleValue)
                .average()
                .orElse(0.0);
    }

    public double[] getTenPercent() {
        int tenPercent = (int) Math.ceil(count * 0.1);
        return this.list.stream()
                .sorted(Comparator.reverseOrder())
                .mapToDouble(Double::doubleValue)
                .limit(tenPercent)
                .toArray();

    }
}
