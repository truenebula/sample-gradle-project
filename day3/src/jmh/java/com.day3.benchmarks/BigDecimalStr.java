package com.day3.benchmarks;

import java.util.List;
import java.util.concurrent.TimeUnit;

import com.example.BigDecimalStreams;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;


@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 20, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(1)
@State(Scope.Benchmark)
public class BigDecimalStr {
    BigDecimalStreams bds = new BigDecimalStreams(List.of(
            new java.math.BigDecimal("1.1"),
            new java.math.BigDecimal("2.3"),
            new java.math.BigDecimal("4.34"),
            new java.math.BigDecimal("22.65"),
            new java.math.BigDecimal("9.9"),
            new java.math.BigDecimal("11.111")
    ));

    @Benchmark @BenchmarkMode(Mode.Throughput)
    public void sum(Blackhole consumer) {
        consumer.consume(bds.getSum());
    }

    @Benchmark @BenchmarkMode(Mode.Throughput)
    public void avg(Blackhole consumer) {
        consumer.consume(bds.getAvg());
    }

    @Benchmark @BenchmarkMode(Mode.Throughput)
    public void tenPercent(Blackhole consumer) {
        consumer.consume(bds.getTenPercent());
    }
}
