package com.day3.benchmarks;

import java.util.List;
import java.util.concurrent.TimeUnit;

import com.example.DoubleStreams;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;


@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 20, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(1)
@State(Scope.Benchmark)
public class DoubleStr {
    DoubleStreams bds = new DoubleStreams(List.of(
            1.1,
            2.3,
            4.34,
            22.65,
            9.9,
            11.111
    ));

    @Benchmark @BenchmarkMode(Mode.Throughput)
    public void sum(Blackhole consumer) {
        consumer.consume(bds.getSum());
    }

    @Benchmark @BenchmarkMode(Mode.Throughput)
    public void avg(Blackhole consumer) {
        consumer.consume(bds.getAvg());
    }

    @Benchmark @BenchmarkMode(Mode.Throughput)
    public void tenPercent(Blackhole consumer) {
        consumer.consume(bds.getTenPercent());
    }
}
