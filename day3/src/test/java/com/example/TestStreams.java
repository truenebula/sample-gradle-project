package com.example;


import org.hamcrest.MatcherAssert;
import static org.hamcrest.Matchers.is;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class TestStreams {
    public BigDecimalStreams bds;
    public DoubleStreams ds;
    public DoublePrimitiveStreams dps;

    @BeforeEach
    public void setup() {
        bds = new BigDecimalStreams(List.of(
                new BigDecimal("1.1"),
                new BigDecimal("2.3"),
                new BigDecimal("4.34"),
                new BigDecimal("22.65"),
                new BigDecimal("9.9"),
                new BigDecimal("11.111")
        ));
        ds = new DoubleStreams(List.of(
                1.1,
                2.3,
                4.34,
                22.65,
                9.9,
                11.111
        ));
        dps = new DoublePrimitiveStreams(List.of(
                1.1,
                2.3,
                4.34,
                22.65,
                9.9,
                11.111
        ));
    }

    @Test
    public void testSum() {
        MatcherAssert.assertThat(bds.getSum().toString(), is("51.401"));
        MatcherAssert.assertThat(ds.getSum().toString(), is("51.400999999999996"));
        MatcherAssert.assertThat(dps.getSum(), is(51.401));
    }

    @Test
    public void testAvg() {
        MatcherAssert.assertThat(bds.getAvg().toString(), is("8.57"));
        MatcherAssert.assertThat(ds.getAvg(), is(8.566833333333333));
        MatcherAssert.assertThat(dps.getAvg(), is(8.566833333333333));
    }

    @Test
    public void testTen() {
        MatcherAssert.assertThat(bds.getTenPercent().toString(), is("[22.65]"));
        MatcherAssert.assertThat(ds.getTenPercent().toString(), is("[22.65]"));
        MatcherAssert.assertThat(Arrays.toString(dps.getTenPercent()), is("[22.65]"));
    }

}
