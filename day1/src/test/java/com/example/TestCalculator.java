package com.example;


import com.example.operation.Addition;
import com.example.operation.Division;
import com.example.operation.Multiplication;
import com.example.operation.Subtraction;

import org.hamcrest.MatcherAssert;
import static org.hamcrest.Matchers.is;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestCalculator {
    public Addition addition;
    public Subtraction subtraction;
    public Multiplication multiplication;
    public Division division;

    @BeforeEach
    public void setup() {
        addition = new Addition();
        subtraction = new Subtraction();
        multiplication = new Multiplication();
        division = new Division();
    }

    @Test
    public void testAddition() {
        MatcherAssert.assertThat(addition.execute(1, 2), is(3));
    }

    @Test
    public void testSubtraction() {
        MatcherAssert.assertThat(subtraction.execute(10, 3), is(7));
    }

    @Test
    public void testMultiplication() {
        MatcherAssert.assertThat(multiplication.execute(3, 4), is(12));
    }

    @Test
    public void testDivision() {
        MatcherAssert.assertThat(division.execute(15, 3), is(5));
    }


}
