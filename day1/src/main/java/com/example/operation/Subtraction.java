package com.example.operation;

public class Subtraction extends Operation{
    @Override
    public int execute(int operand1, int operand2) {
        return operand1 - operand2;
    }
}
