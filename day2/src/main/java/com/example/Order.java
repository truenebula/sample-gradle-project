package com.example;

public class Order implements Comparable<Order> {
    private int id;
    private int price;
    private int quantity;

    public Order(int id, int price, int quantity) {
        this.id = id;
        this.price = price;
        this.quantity = quantity;
    }

    public boolean equals(Order o) {
        return this.id == o.id;
    }

    public int hashCode() {
        return this.id;
    }

    public String toString() {
        return "Order: " + this.id + " -> " + this.price + " x " + this.quantity;
    }

    public int compareTo(Order o) {
        return this.id - o.id;
    }

    public Integer getId() {
        return this.id;
    }

    public Integer getPrice() {
        return this.price;
    }

    public Integer getQuantity() {
        return this.quantity;
    }

}
