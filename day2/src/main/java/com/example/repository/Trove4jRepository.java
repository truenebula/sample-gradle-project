package com.example.repository;


import com.example.Order;
import gnu.trove.set.hash.THashSet;

public class Trove4jRepository implements InMemoryRepository<Order>{
    private THashSet<Order> orders;

    public Trove4jRepository() {
        this.orders = new THashSet<Order>();
    }

    public void add(Order order) {
        this.orders.add(order);
    }

    public boolean contains(Order order) {
        return this.orders.contains(order);
    }

    public void remove(Order order) {
        this.orders.remove(order);
    }
}
