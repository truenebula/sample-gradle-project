package com.example.repository;

import com.example.Order;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapRepository implements InMemoryRepository<Order>{
    private ConcurrentHashMap<Integer, Order> orders;

    public ConcurrentHashMapRepository() {
        this.orders = new ConcurrentHashMap<Integer, Order>();
    }

    public void add(Order order) {
        this.orders.put(order.getId(), order);
    }

    public boolean contains(Order order) {
        return this.orders.contains(order);
    }

    public void remove(Order order) {
        this.orders.remove(order);
    }
}
