package com.example.repository;

import org.eclipse.collections.api.map.MutableMap;
import org.eclipse.collections.impl.map.mutable.UnifiedMap;
import com.example.Order;


public class EclipseCollectionsRepository implements InMemoryRepository<Order>{
    private MutableMap<Integer, Order> orders;

    public EclipseCollectionsRepository() {
        this.orders = new UnifiedMap<Integer, Order>();
    }

    public void add(Order order) {
        this.orders.put(order.getId(), order);
    }

    public boolean contains(Order order) {
        return this.orders.contains(order);
    }

    public void remove(Order order) {
        this.orders.remove(order);
    }
}
