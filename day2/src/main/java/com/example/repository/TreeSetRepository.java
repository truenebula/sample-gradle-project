package com.example.repository;

import com.example.Order;

import java.util.TreeSet;

public class TreeSetRepository implements InMemoryRepository<Order>{
    private TreeSet<Order> orders;

    public TreeSetRepository() {
        this.orders = new TreeSet<Order>();
    }

    public void add(Order order) {
        this.orders.add(order);
    }

    public boolean contains(Order order) {
        return this.orders.contains(order);
    }

    public void remove(Order order) {
        this.orders.remove(order);
    }
}
