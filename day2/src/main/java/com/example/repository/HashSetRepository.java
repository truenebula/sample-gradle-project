package com.example.repository;

import com.example.Order;
import java.util.HashSet;

public class HashSetRepository implements InMemoryRepository<Order> {
    private HashSet<Order> orders;

    public HashSetRepository() {
        this.orders = new HashSet<Order>();
    }

    public void add(Order order) {
        this.orders.add(order);
    }

    public boolean contains(Order order) {
        return this.orders.contains(order);
    }

    public void remove(Order order) {
        this.orders.remove(order);
    }
}
