package com.example.repository;

import com.example.Order;
import java.util.ArrayList;

public class ArrayListRepository implements InMemoryRepository<Order>{
    private ArrayList<Order> orders;

    public ArrayListRepository() {
        this.orders = new ArrayList<Order>();
    }

    public void add(Order order) {
        this.orders.add(order);
    }

    public boolean contains(Order order) {
        return this.orders.contains(order);
    }

    public void remove(Order order) {
        this.orders.remove(order);
    }
}
