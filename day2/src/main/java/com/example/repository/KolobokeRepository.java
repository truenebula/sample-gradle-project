package com.example.repository;

import com.koloboke.collect.set.hash.HashObjSet;
import com.example.Order;

public class KolobokeRepository implements InMemoryRepository<Order>{
    private HashObjSet<Order> orders;

    public KolobokeRepository() {
        this.orders = com.koloboke.collect.set.hash.HashObjSets.newMutableSet();
    }

    public void add(Order order) {
        this.orders.add(order);
    }

    public boolean contains(Order order) {
        return this.orders.contains(order);
    }

    public void remove(Order order) {
        this.orders.remove(order);
    }
}
