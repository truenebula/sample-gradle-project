package com.day2.benchmarks;

import java.util.concurrent.TimeUnit;

import com.example.Order;
import com.example.repository.TreeSetRepository;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;


@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 20, time = 1, timeUnit = TimeUnit.SECONDS)
@Fork(1)
@State(Scope.Benchmark)
public class TreeSetRepo {
    TreeSetRepository repository = new TreeSetRepository();

    @Benchmark @BenchmarkMode(Mode.Throughput)
    public void add() {
        repository.add(new Order(1, 20, 1));
        repository.add(new Order(2, 50, 3));
        repository.add(new Order(3, 70, 4));
    }

    @Benchmark @BenchmarkMode(Mode.Throughput)
    public void contains(Blackhole consumer) {
        consumer.consume(repository.contains(new Order(1, 20, 1)));
        consumer.consume(repository.contains(new Order(2, 20, 1)));
        consumer.consume(repository.contains(new Order(3, 70, 4)));
    }

    @Benchmark @BenchmarkMode(Mode.Throughput)
    public void remove() {
        repository.remove(new Order(1, 20, 1));
        repository.remove(new Order(2, 50, 3));
        repository.remove(new Order(3, 70, 4));
    }
}
